# Makefile for complete build

PROGRAMM_NAME := tbmon2

CORE_PATH := core/
ANALYSIS_PATH := analysis/
PREPROCESS_PATH := preprocess/
CORE_HEADER_PATH := core/include/
STYLE_HEADER_PATH := style/include/
LIB_PATH := lib/
BUILD_PATH := .build/

MAIN_FILE := $(wildcard *.cc)
MAIN_OBJ := $(patsubst %.cc, $(BUILD_PATH)%.o, $(MAIN_FILE))

ECHO := echo
RM := rm
MKDIR := mkdir
MV := mv

CPP := c++
CPPFLAGS := -ldl -fPIC -g -Werror

CERN_ROOT_FLAGS := $(shell root-config --cflags)
CERN_ROOT_LINK := $(shell root-config --glibs)
TBMON2_INCLUDE := -I$(CORE_HEADER_PATH) -I$(STYLE_HEADER_PATH) -I$(LIB_PATH)libconfig-1.4.9/lib/
TBMON2_LIBS := $(LIB_PATH)libconfig-1.4.9/lib/.libs/libconfig++.a

.PHONY: build rebuild rebuildall cleanall cleandict core analysis preprocess

.SUFFIXES: .o .cc

build: $(PROGRAMM_NAME) analysis preprocess
	@if [ -d $(BUILD_PATH) ]; \
	then \
		$(ECHO) "Compilation done."; \
	else \
		$(ECHO) "Nothing to do."; \
	fi
	
$(PROGRAMM_NAME): core $(MAIN_OBJ)
	$(CPP) $(CPPFLAGS) -o $@ $(MAIN_OBJ) $(CORE_PATH)$(BUILD_PATH)*.o $(TBMON2_LIBS) $(CERN_ROOT_LINK)

core: 
	$(MAKE) -C $(CORE_PATH)
	
$(BUILD_PATH)%.o: %.cc
	@if ! [ -d $(BUILD_PATH) ]; \
	then \
		$(MKDIR) -p $(BUILD_PATH);\
	fi
	$(CPP) $(CPPFLAGS) $(CERN_ROOT_FLAGS) $(TBMON2_INCLUDE) -std=c++11 -c -MMD -MP -MF $@.d -o $@ $<
	
analysis:
	$(MAKE) -C $(ANALYSIS_PATH)

preprocess:
	$(MAKE) -C $(PREPROCESS_PATH)
	
rebuild: clean build
	
rebuildcore: clean build
	
rebuildanalysis: cleananalysis analysis
	
rebuildpreprocess: cleanpreprocess preprocess
	
rebuildall: cleanall build

clean:
	@if [ -d $(BUILD_PATH) ]; \
	then \
		$(RM) -r $(BUILD_PATH); \
		$(ECHO) "Clean up done."; \
	else \
		$(ECHO) "Nothing to do."; \
	fi
	@if [ -e $(PROGRAMM_NAME) ]; \
	then \
		$(RM) $(PROGRAMM_NAME); \
	fi
	
cleancore:
	$(MAKE) cleanall -C $(CORE_PATH)
	
cleananalysis:
	$(MAKE) cleanall -C $(ANALYSIS_PATH)

cleanpreprocess:
	$(MAKE) cleanall -C $(PREPROCESS_PATH)
	
cleanall: clean
	$(MAKE) $@ -C $(CORE_PATH)
	$(MAKE) $@ -C $(ANALYSIS_PATH)
	$(MAKE) $@ -C $(PREPROCESS_PATH)
	
cleandict:
	$(MAKE) cleandict -C $(CORE_PATH)
	
