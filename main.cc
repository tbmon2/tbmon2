#include <sys/stat.h>
#include <sys/types.h>

#include <cstdlib>
#include <string.h>
#include <iostream>
#include <utility>
#include <errno.h>
#include <unistd.h>
#include <algorithm>
#include <fstream>

#include "libconfig.h++"
#include "TBConfig.h"
#include "TBDutConfig.h"
#include "TBCore.h"

#define MAINCONFIGNAME "mainConfig.cfg"
#define DUTConfig "config/DUTConfigTest.cfg"


using namespace std;
using namespace libconfig;

int checkPath(const string& path);
int createPath(const string& rootPath, const string& path, mode_t mode);
void copyFile(const string& filePath, const string& targetPath);

// load one config file
int openConfig(Config* cfg, string* name)
{
	try
	{
		(*cfg).readFile((*name).c_str());
	}
	catch(const FileIOException &fioex)
	{
		cerr << "[ TBmon ]: I/O error while reading file. Maybe it does not exist.\n" << endl;
		return(EXIT_FAILURE);
	}
	catch(const ParseException &pex)
	{
		cerr << "[ TBmon ]: Parse error at " << pex.getFile() << ":" << pex.getLine()
              << " - " << pex.getError() << endl;
		return(EXIT_FAILURE);
	}
}

// check path on unix-system with POSIX macros
int checkPath(const string& path)
{
	struct stat st;
	int out = stat(path.c_str(), &st);
	
	if(!S_ISDIR(st.st_mode))
	{
		cout << "[ TBmon ]: [" << path <<  "] does not exists" << endl;
		cout << "[ TBmon ]: " << "Create [" << path <<  "] and copy default config files" << endl;
		createPath("", path, 0744);
		createPath(path, TBConfig::configPath, 0744);
		createPath(path, TBConfig::preprocessingDataPath, 0744);
		createPath(path, TBConfig::plotPath, 0744);
		createPath(path, TBConfig::extMaskPath, 0744);
		copyFile((TBConfig::draftConfigPath+TBConfig::mainConfigName), (path+"/"+TBConfig::configPath+"/"+TBConfig::mainConfigName));
		copyFile((TBConfig::draftConfigPath+TBConfig::analysisConfigName), (path+"/"+TBConfig::configPath+"/"+TBConfig::analysisConfigName));
		cout << "[ TBmon ]: do not forget to edit the config files in [" << path << "]" << endl;
		
		return 0;
	}	
	else
	{
		cout << "[ TBmon ]: [" << path <<  "] is already exists" << endl;
		return 1;
	}
}

int createPath(const string& rootPath, const string& path, mode_t mode)
{
	struct stat st;
	
	for(string::const_iterator it = path.begin(); it != path.end();)
	{
		string::const_iterator setIt = find(it, path.end(), '/');
		string setPath = rootPath+"/"+string(path.begin(), setIt);
		
		if(stat(setPath.c_str(), &st) != 0)
		{
			if(mkdir(setPath.c_str(), mode) != 0 && errno != EEXIST)
			{
				cout << "[ TBmon ]: cannot create folder [" << setPath << "] :" << strerror(errno) << endl;
				return -1;
			}
		}
		else
			if(!S_ISDIR(st.st_mode))
			{
				errno = ENOTDIR;
				cout << "[ TBmon ]: path [" << setPath << "] not a dir" << endl;
				return -1;
			}
		
		it = setIt;
		if(setIt != path.end())
		{
			it++;
		}
	}
	
	return 0;
}

// copy file
void copyFile(const string& filePath, const string& targetPath)
{
	ifstream f1 (filePath.c_str(), fstream::binary);
	ofstream f2 (targetPath.c_str(), fstream::trunc|fstream::binary);
	f2 << f1.rdbuf();
}

int main(int argc, char** argv)
{
	string outputPath;
	TBConfig tbconfig;
	if(argc < 2)
	{
		cout << "[ TBmon ]: Please enter an output path:" << endl;
		cin >> outputPath;
	}
	else
	{
		outputPath.append(argv[1]);
	}
	
	if(!checkPath(outputPath))
	{
		cout << "[ TBmon ]: done without analysis" << endl;
		
		return(EXIT_SUCCESS);
	}
	else
	{
		tbconfig.outputPath = ((outputPath.back()) == '/') ? outputPath + "" : outputPath + "/";
		cout << "[ TBmon ]: continue with analysis" << endl;
	}

	int count;
	int count2;
	int count3;
	// Name of the main config file
	string mainCfgName = tbconfig.outputPath+TBConfig::configPath+TBConfig::mainConfigName;
	
	// load mainConfig
	Config mainCfg;
	
	openConfig(&mainCfg, &mainCfgName);
	Setting& mainCfgRoot = mainCfg.getRoot();
	
	// set all settings from the mainConfig file into TBConfig object
	try
	{
		const Setting& settings = mainCfgRoot["settings"];

		settings.lookupValue("rawDataPath", tbconfig.rawDataPath);
		settings.lookupValue("rawDataName", tbconfig.rawDataName);
		
		// build raw data run list from array in the config file
		count = settings["rawDataRuns"].getLength();
		for(int i=0; i<count; i++)
		{
			if(i+1 < count && int(settings["rawDataRuns"][i]) >= 0 && int(settings["rawDataRuns"][i+1]) < 0)
			{
					for(int j = int(settings["rawDataRuns"][i]); j <= abs(int(settings["rawDataRuns"][i+1])); j++)
					{
						tbconfig.rawDataRuns.push_back(j);
					}
			}
			else
			{
				if(int(settings["rawDataRuns"][i]) >= 0)
				{
					tbconfig.rawDataRuns.push_back(int(settings["rawDataRuns"][i]));
				}
			}
		}
		
		settings.lookupValue("maxNumOfTrigToBeProcPerRun", tbconfig.maxNumOfTrigToBeProcPerRun);
		
		settings.lookupValue("logLevel", tbconfig.logLevel);
		settings.lookupValue("organizeOutput", tbconfig.organizeOutput);
		settings.lookupValue("logToFile", tbconfig.logToFile);
		
		settings.lookupValue("plotExtension", tbconfig.plotExtension);
		
		settings.lookupValue("savePreprocessingFile", tbconfig.savePreprocessingFile);
		settings.lookupValue("addingPreprocessingFile", tbconfig.addingPreprocessingFile);
		settings.lookupValue("usePreprocessingFile", tbconfig.usePreprocessingFile);
		
		settings.lookupValue("overwriteFiles", tbconfig.overwriteFiles);

		settings.lookupValue("useRecoZ", tbconfig.useRecoZ);

		settings.lookupValue("recoGEAR", tbconfig.usedGEAR);
		
		settings.lookupValue("useAtlasStyle", tbconfig.useAtlasStyle);

		count = settings["useAnalyses"].getLength();
		for(int i=0; i<count; i++)
		{
			tbconfig.useAnalysis.push_back(settings["useAnalyses"][i]);
		}
		// setup all DUTs
		pair<string, string> p;
		// TODO
		for(int i=0; i<settings["useDUT"].getLength(); i++)
		{
			string dutName = settings["useDUT"][i]["DUTName"];
			string dutConfig = settings["useDUT"][i]["DUTConfigFile"];
			p = make_pair(dutName, dutConfig);
			tbconfig.useDUT.insert(p);
			
			// Name of the DUTConfig file
			string dutCfgName = tbconfig.generalDutConfigPath+dutConfig;

			// load dutConfig
			Config dutCfg;
			TBDutConfig tbdutconfig;
			openConfig(&dutCfg, &dutCfgName);
			Setting& dutCfgRoot = dutCfg.getRoot();

			// set all settings from the mainConfig file into TBConfig object
			try
			{
				const Setting& mainDut = dutCfgRoot["mainDUTConfig"];

				tbdutconfig.DUTName = dutName;

				mainDut.lookupValue("DUTsizeX", tbdutconfig.DUTsizeX);
				mainDut.lookupValue("DUTsizeY", tbdutconfig.DUTsizeY);
				mainDut.lookupValue("DUTthickness", tbdutconfig.DUTthickness);
				tbdutconfig.iden = settings["useDUT"][i]["iden"];
				mainDut.lookupValue("cols", tbdutconfig.cols);
				mainDut.lookupValue("rows", tbdutconfig.rows);
				mainDut.lookupValue("totcalibPath", tbdutconfig.totcalibPath);
				mainDut.lookupValue("maxTot", tbdutconfig.maxTot);
				// TEST delete later 
				mainDut.lookupValue("numElec", tbdutconfig.numElec);
				

				tbdutconfig.lv1Min = settings["useDUT"][i]["lv1Min"];
				tbdutconfig.lv1Max = settings["useDUT"][i]["lv1Max"];
				
				tbdutconfig.maskNeighborsColRadius = settings["useDUT"][i]["maskNeighborsColRadius"];
				tbdutconfig.maskNeighborsRowRadius = settings["useDUT"][i]["maskNeighborsRowRadius"];

				tbdutconfig.maskDeadPixels = settings["useDUT"][i]["maskDeadPixels"];

				if (not tbconfig.useRecoZ)
				{
					tbdutconfig.zPos = settings["useDUT"][i]["zPos"];
				}
				
				tbdutconfig.matchX = settings["useDUT"][i]["matchX"];
				tbdutconfig.matchY = settings["useDUT"][i]["matchY"];
				
				tbdutconfig.matchPixelMarginX = settings["useDUT"][i]["matchPixelMarginX"];
				tbdutconfig.matchPixelMarginY = settings["useDUT"][i]["matchPixelMarginY"];

				// build mask col array from array in the config file
				count = settings["useDUT"][i]["maskCol"].getLength();
				for(int k=0; k<count; k++)
				{
					if(k+1 < count && int(settings["useDUT"][i]["maskCol"][k]) >= 0 && int(settings["useDUT"][i]["maskCol"][k+1]) < 0)
					{
							for(int j = int(settings["useDUT"][i]["maskCol"][k]); j <= abs(int(settings["useDUT"][i]["maskCol"][k+1])); j++)
							{
								tbdutconfig.maskCol.push_back(j);
							}
					}
					else
					{
						if(int(settings["useDUT"][i]["maskCol"][k]) >= 0)
						{
							tbdutconfig.maskCol.push_back(int(settings["useDUT"][i]["maskCol"][k]));
						}
					}
				}

				// build mask row array from array in the config file
				count = settings["useDUT"][i]["maskRow"].getLength();
				for(int k=0; k<count; k++)
				{
					if(k+1 < count && int(settings["useDUT"][i]["maskRow"][k]) >= 0 && int(settings["useDUT"][i]["maskRow"][k+1]) < 0)
					{
							for(int j = int(settings["useDUT"][i]["maskRow"][k]); j <= abs(int(settings["useDUT"][i]["maskRow"][k+1])); j++)
							{
								tbdutconfig.maskRow.push_back(j);
							}
					}
					else
					{
						if(int(settings["useDUT"][i]["maskRow"][k]) >= 0)
						{
							tbdutconfig.maskRow.push_back(int(settings["useDUT"][i]["maskRow"][k]));
						}
					}
				}

				// setup eventbuilder params for each dut
				count = settings["useDUT"][i]["eventBuilderParam"].getLength();
				for(int k=0; k<count; k++)
				{
					count2 = settings["useDUT"][i]["eventBuilderParam"][k].getLength();
					for(int j=0; j<count2; j++)
					{						
						tbconfig.param[tbdutconfig.iden][string(settings["useDUT"][i]["eventBuilderParam"][k].getName())][string(settings["useDUT"][i]["eventBuilderParam"][k][j].getName())] = string(settings["useDUT"][i]["eventBuilderParam"][k][j].c_str());
					}
				}
				
				count = dutCfgRoot["pixelConfig"].getLength();
				for(int l=0; l<count; l++)
				{
					TBPixelConfig pixel;

					pixel.edge = dutCfgRoot["pixelConfig"][l]["edge"];
					pixel.numElec = dutCfgRoot["pixelConfig"][l]["numElec"];

					pixel.firstPixelCol = dutCfgRoot["pixelConfig"][l]["firstPixelCol"];
					pixel.firstPixelRow = dutCfgRoot["pixelConfig"][l]["firstPixelRow"];	
					pixel.firstPixelX = dutCfgRoot["pixelConfig"][l]["firstPixelX"];
					pixel.firstPixelY = dutCfgRoot["pixelConfig"][l]["firstPixelY"];

					pixel.periodPixelCol = dutCfgRoot["pixelConfig"][l]["periodPixelCol"];
					pixel.periodPixelRow = dutCfgRoot["pixelConfig"][l]["periodPixelRow"];
					pixel.periodPixelX = dutCfgRoot["pixelConfig"][l]["periodPixelX"];
					pixel.periodPixelY = dutCfgRoot["pixelConfig"][l]["periodPixelY"];

					pixel.countPixelCol = dutCfgRoot["pixelConfig"][l]["countPixelCol"];
					pixel.countPixelRow = dutCfgRoot["pixelConfig"][l]["countPixelRow"];
					
					pixel.periodMirroringCol = dutCfgRoot["pixelConfig"][l]["periodMirroringCol"];

					count2 = dutCfgRoot["pixelConfig"][l]["rectangleConfig"].getLength();
					for(int j=0; j<count2; j++)
					{
						TBRectangleConfig rectangle;

						rectangle.lowerLeftX = dutCfgRoot["pixelConfig"][l]["rectangleConfig"][j]["lowerLeftX"];
						rectangle.lowerLeftY = dutCfgRoot["pixelConfig"][l]["rectangleConfig"][j]["lowerLeftY"];

						rectangle.upperRightX = dutCfgRoot["pixelConfig"][l]["rectangleConfig"][j]["upperRightX"];
						rectangle.upperRightY = dutCfgRoot["pixelConfig"][l]["rectangleConfig"][j]["upperRightY"];

						pixel.rectangle.push_back(rectangle);
					}

					tbdutconfig.pixel.push_back(pixel);

				}

				tbconfig.dutConfig.push_back(tbdutconfig);
				
				// clean up this scope
				//delete mainDut;
			}
			catch(const SettingNotFoundException &nfex)
			{
				cout << nfex.getPath() << " Does not exist in " << dutCfgName << "." << endl;
				return(EXIT_FAILURE);
			}
			
			// clean up this scope
			//delete &dutCfg;
			//delete dutCfgRoot;
		}
		// ---------------
		count = mainCfgRoot["eventBuilderParam"].getLength();
		for(int k=0; k<count; k++)
		{
			count2 = mainCfgRoot["eventBuilderParam"][k].getLength();
			for(int j=0; j<count2; j++)
			{
				tbconfig.param[-1][string(mainCfgRoot["eventBuilderParam"][k].getName())][string(mainCfgRoot["eventBuilderParam"][k][j].getName())] = string(mainCfgRoot["eventBuilderParam"][k][j].c_str());
			}
		}
		
		// ---------------
		const Setting& preprocess = mainCfgRoot["preprocessing"];
		count = preprocess.getLength();
		preprocess.lookupValue("useCheckAlign", tbconfig.useCheckAlign);
		preprocess.lookupValue("useGetEtaCorr", tbconfig.useGetEtaCorr);
	}
	catch(const SettingNotFoundException &nfex)
	{
		cout << nfex.getPath() << " Does not exist in " << mainCfgName << "." << endl;
		return(EXIT_FAILURE);
	}
	catch (const SettingTypeException &nfex)
	  {
	    cout <<  nfex.getPath() << " Does not exist in " << mainCfgName << "." << endl;	     
	  }
	
	// Name of the analysis config file
	string analysisCfgName = tbconfig.outputPath+"/"+TBConfig::configPath+TBConfig::analysisConfigName;
	
	// load analysisConfig
	Config analysisCfg;
	
	openConfig(&analysisCfg, &analysisCfgName);
	Setting& analysisCfgRoot = analysisCfg.getRoot();
	
	// set all settings from the analysisConfig file into TBConfig multi-multimap param
	try
	{
		count = analysisCfgRoot.getLength();
		for(int i=0; i<count; i++)
		{
			count2 = analysisCfgRoot[i]["configList"].getLength();
			for(int k=0; k<count2; k++)
			{
				count3 = analysisCfgRoot[i]["configList"][k].getLength();
				for(int j=1; j<count3; j++)
				{
					tbconfig.param[analysisCfgRoot[i]["configList"][k]["DUTiden"]][string(analysisCfgRoot[i].getName())][string(analysisCfgRoot[i]["configList"][k][j].getName())] = string(analysisCfgRoot[i]["configList"][k][j].c_str());
				}
			}
		}
		
	}
	catch(const SettingNotFoundException &nfex)
	{
		cout << nfex.getPath() << " Does not exist in " << analysisCfgName << "." << endl;
		return(EXIT_FAILURE);
	}
	
	// clean up this scope
	//delete &mainCfg;
	//delete mainCfgRoot;
	//delete &analysisCfg;
	//delete analysisCfgRoot;
	
	// ----------------------------------
	
	// run the TBCore
	TBCore* core = new TBCore(&tbconfig);
	core->loop();
	
	delete core;
	cout << "[ TBmon ]: Done." << endl;
	
	return(EXIT_SUCCESS);
}
