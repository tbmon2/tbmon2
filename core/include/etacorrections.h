#ifndef ETACORRECTIONS_H
#define ETACORRECTIONS_H

#include <cmath>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>

using namespace std;
class EtaCorrections {
public:
  struct EtaCorr{
    char* name;
    int firstRun;
    int lastRun;
    vector<double> ecorrX;
    vector<double> ecorrY;
    int _nbins;
  };
private:
  vector<EtaCorr> allCorrs;
public:
  EtaCorr eCorrs;
  void initRun(int currentRun);
  void addEcorr(const char* fileName, int nbins=100);
  double getX(double chargeCorrected);
  double getY(double chargeCorrected);
};

#endif //ETACORRECTIONS_H
