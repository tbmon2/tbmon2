#include "totcalib.h"

ToTCalib::ToTCalib(std::string filename){
  // loads the function and histograms stored in a root file
  // requires one histogram for each function's free parameter
  perPixelCalib = true;
  histLoaded = true;
  h_par.clear();

  TFile* file = new TFile(filename.c_str());
  file->cd();
  function = dynamic_cast <TF1*> (file->Get("ToT_calib_function"));
  if (function == 0){
    histLoaded = false;
  }
  else{
    for (int i = 0; i < function->GetNumberFreeParameters(); i++){
      // get the histograms stored under the name "hpar_0" and so on
      std::string histName = "hpar_";
      histName += std::to_string(i);
      TH2F *h_parameter = (dynamic_cast <TH2F*> (file->Get(histName.c_str())));
      h_parameter->SetDirectory(gROOT);
      
      if (h_parameter == 0){
	std::cout << "No Histogram for totcalib found!" << __FILE__<<":"<<__LINE__<<std::endl;
	histLoaded = false;
      }
      h_par.push_back(h_parameter);
    }
    if (h_par.size() != static_cast <unsigned int> (function->GetNumberFreeParameters())){
      histLoaded = false;
    }
  }
  file->Close();
}
 
double ToTCalib::q(int tot, int col, int row){
  // if a calib is existing
  if (hasCalib()){
    // get the parameters out of the histogram into the function
    for (unsigned int i = 0; i < h_par.size(); i++){
      function->SetParameter(i,double(h_par[i]->GetBinContent(col+1,row+1))); // parse dut col/row to bin count
    }
    // evaluate the function for a certain tot
    return function->Eval(double(tot));
  }
  else{
    return -1;
  }
}

ToTCalib::~ToTCalib() {}
