/* 
 * File:   ClusterMatcher.h
 * Author: daniel
 *
 * Created on 9. Februar 2014, 22:16
 */

#ifndef CLUSTERMATCHER_H
#define	CLUSTERMATCHER_H

#include "TBPreprocess.h"

class ClusterMatcher : public TBPreprocess
{
public:
	ClusterMatcher()
	{
		TBPreprocess::name = "ClusterMatcher";
	}
	virtual void buildEvent(TBCore* core, TBEvent* event);
};

// class factories
extern "C" TBPreprocess* create()
{
	return new ClusterMatcher;
}

extern "C" void destroy(TBPreprocess* tbp)
{
	delete tbp;
}

#endif	/* CLUSTERMATCHER_H */

